import imp
import os
import sys


sys.path.insert(0, os.path.dirname(__file__))

# wsgi = imp.load_source('wsgi', 'main.py')
# application = wsgi.wsgi.py
def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain')])
    message = 'wow It works!\n'
    version = 'py v' + sys.version.split()[0] + '\n'
    response = '\n'.join([message, version])
    return [response.encode()]